import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SstRoutingModule } from './sst-routing.module';
import { BookingComponent } from './Bookings/booking/booking.component';
import { AllBookingComponent } from './Bookings/all-booking/all-booking.component';
import { AllLandsComponent } from './Lands/all-lands/all-lands.component';
import { AddLandsComponent } from './Lands/add-lands/add-lands.component';
import { DetailsLandComponent } from './Lands/details-land/details-land.component';
import { AllUserComponent } from './Users/all-user/all-user.component';
import { AddUserComponent } from './Users/add-user/add-user.component';
import { AllLeadComponent } from './Leads/all-lead/all-lead.component';
import { AddLeadComponent } from './Leads/add-lead/add-lead.component';
import { DetailsBookingComponent } from './Bookings/details-booking/details-booking.component';
import { DetailsLeadComponent } from './Leads/details-lead/details-lead.component';
import { DetailsUserComponent } from './Users/details-user/details-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AllCustomersComponent } from './Customer/all-customers/all-customers.component';
import { DetailsCustomersComponent } from './Customer/details-customers/details-customers.component';
import { CustomersComponent } from './Customer/customers/customers.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AllMediatorComponent } from './Mediators/all-mediator/all-mediator.component';
import { DetailsMediatorComponent } from './Mediators/details-mediator/details-mediator.component';
import { MediatorComponent } from './Mediators/mediator/mediator.component';
import { AllProjectexpensesComponent } from './ProjectExpenses/all-projectexpenses/all-projectexpenses.component';
import { DetailsProjectexpensesComponent } from './ProjectExpenses/details-projectexpenses/details-projectexpenses.component';
import { ProjectexpensesComponent } from './ProjectExpenses/projectexpenses/projectexpenses.component';
import { AllSalaryexpensesComponent } from './Salaryexpenses/all-salaryexpenses/all-salaryexpenses.component';
import { SalaryexpensesComponent } from './Salaryexpenses/salaryexpenses/salaryexpenses.component';
import { DetailsSalaryexpensesComponent } from './Salaryexpenses/details-salaryexpenses/details-salaryexpenses.component';
import { AllOfficeexpensesComponent } from './Officeexpenses/all-officeexpenses/all-officeexpenses.component';
import { DetailsOfficeexpensesComponent } from './Officeexpenses/details-officeexpenses/details-officeexpenses.component';
import { OfficeexpensesComponent } from './Officeexpenses/officeexpenses/officeexpenses.component';
import { AllCourtexpensesComponent } from './Courtexpenses/all-courtexpenses/all-courtexpenses.component';
import { DetailsCourtexpensesComponent } from './Courtexpenses/details-courtexpenses/details-courtexpenses.component';
import { CourtexpensesComponent } from './Courtexpenses/courtexpenses/courtexpenses.component';
import { LoginComponent } from './Login/login/login.component';
import { SignupComponent } from './Login/signup/signup.component';


@NgModule({
  declarations: [BookingComponent, AllBookingComponent, AllLandsComponent, AddLandsComponent, DetailsLandComponent, AllUserComponent, AddUserComponent, AllLeadComponent, AddLeadComponent, DetailsBookingComponent, DetailsLeadComponent, DetailsUserComponent, CustomersComponent, AllCustomersComponent, DetailsCustomersComponent, MediatorComponent, AllMediatorComponent, DetailsMediatorComponent, AllProjectexpensesComponent, DetailsProjectexpensesComponent, ProjectexpensesComponent, AllSalaryexpensesComponent, SalaryexpensesComponent, DetailsSalaryexpensesComponent, AllOfficeexpensesComponent, DetailsOfficeexpensesComponent, OfficeexpensesComponent, AllCourtexpensesComponent, DetailsCourtexpensesComponent, CourtexpensesComponent, LoginComponent, SignupComponent],
  imports: [
    CommonModule,
    SstRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
    Ng2SearchPipeModule
  ],
})
export class SstModule { }
