import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from 'src/app/Services/Booking/booking.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-booking',
  templateUrl: './details-booking.component.html',
  styleUrls: ['./details-booking.component.scss']
})
export class DetailsBookingComponent implements OnInit {

  bookingDetails: any;
  installmentList:any;

  constructor(private bookingService: BookingService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const bookingId = Number(routeParams.get('id'));
    this.bookingService.getBookingById(bookingId).subscribe(data => {
      console.log(JSON.stringify(data));
      this.bookingDetails = data;
      this.installmentList= this.bookingDetails.installments;
      console.log(JSON.stringify(this.installmentList));
    });
  }

  deleteBookings() {
    this.bookingService.deleteBookingById(this.bookingDetails.id).subscribe(data => {
      console.log("booking deleted successfully" + this.bookingDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteBookings();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.bookingDetails.firstName} booking has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-booking']);
      }
    });
  }
}
