import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from 'src/app/Services/Booking/booking.service';
import Swal from 'sweetalert2';

interface Payment {
  viewValue: string;
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {


  submited = false;
  isUpdate: boolean = false;
  projectData: any;
  userId: any;
  id: string;
  bookingDetailsToPatch;
  buttonVal;
  headerVal;
  isPatching = false;
  isAddedByUser = false;


  dists: any[] = [
    { viewValue: 'CASH'},
    { viewValue: 'NEFT'},
    { viewValue: 'UPI'},
    { viewValue: 'CHEQUE'},
  ];
  selectedItem =this.dists;

  installmentSection = new FormGroup({
    date: new FormControl(''),
    amount: new FormControl(''),
    modeOfPayment: new FormControl(''),
    chequeNo:new FormControl(''),
    id: new FormControl('')
  })


  bookingForm = new FormGroup({
    firstName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(15), Validators.pattern('^[a-zA-Z]+')]),
    middleName: new FormControl(''),
    lastName: new FormControl(''),
    fathersName: new FormControl(''),
    permanentAddress: new FormControl(''),
    correspondenceAddress: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),
    emailId: new FormControl(''),
    mobileNumber: new FormControl('',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
    alternateMobileNumber: new FormControl(''),
    dateOfBirth: new FormControl(''),
    occupation: new FormControl(''),
    mediatorName: new FormControl(''),
    reference: new FormControl(''),
    schemeName: new FormControl(''),
    outright: new FormControl(''),
    installment: new FormControl(''),
    totalCostRS: new FormControl(''),
    advanceRS: new FormControl(''),
    monthlyScheme: new FormControl(''),
    fullPayment: new FormControl(''),
    moneyReceiptNumber: new FormControl(''),
    plotSize: new FormControl(''),
    bookingDate: new FormControl(''),
    agreementDate: new FormControl(''),
    introduced: new FormControl(''),
    verifiedBy: new FormControl(''),
    installmentsList : new FormArray([])
  });
  constructor(private bookingService: BookingService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const bookingId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Booking id " + bookingId);
    if (bookingId != 0) {
      this.headerVal = "Update";
      this.isUpdate = true;
      this.bookingService.getBookingById(bookingId).subscribe(data => {
        this.bookingDetailsToPatch = data;
        console.log('individual booking details patching' + JSON.stringify(this.bookingDetailsToPatch));
        this.patchBookingDetails(this.bookingDetailsToPatch);
        this.isPatching = true;
      })

    }
  }


  booking() {
      console.log('booking data ' + JSON.stringify(this.bookingForm.value));
      this.bookingService.addBooking(this.bookingForm.value).subscribe(data => {
        console.log(data);
      });
  }

  saveBooking() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.booking();
            Swal.fire({
              title: 'Submited',
              text: `Booking form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-booking']);
          }
        });
  }


  updateBookings() {
    console.log('Update booking data '+ JSON.stringify(this.bookingForm.value));
    this.bookingService.updateBooking(this.bookingForm.value, this.bookingDetailsToPatch.id).subscribe(data => {
      console.log("updated booking return value "+ JSON.stringify(data));
    })
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateBookings();
            Swal.fire({
              title: 'Updated',
              text: `Booking has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-booking']);
          }
        });
  }




  patchBookingDetails(toBepatchedData) {

    this.bookingForm.patchValue(
      {
        firstName: toBepatchedData.firstName,
        middleName: toBepatchedData.middleName,
        lastName: toBepatchedData.lastName,
        fathersName: toBepatchedData.fathersName,
        permanentAddress: toBepatchedData.permanentAddress,
        correspondenceAddress: toBepatchedData.correspondenceAddress,
        city: toBepatchedData.city,
        state: toBepatchedData.state,
        postalCode: toBepatchedData.postalCode,
        emailId: toBepatchedData.emailId,
        mobileNumber: toBepatchedData.mobileNumber,
        alternateMobileNumber: toBepatchedData.alternateMobileNumber,
        dateOfBirth: toBepatchedData.dateOfBirth,
        occupation: toBepatchedData.occupation,
        mediatorName: toBepatchedData.mediatorName,
        reference: toBepatchedData.reference,
        schemeName: toBepatchedData.schemeName,
        totalCostRS: toBepatchedData.totalCostRS,
        advanceRS: toBepatchedData.advanceRS,
        monthlyScheme: toBepatchedData.monthlyScheme,
        fullPayment: toBepatchedData.fullPayment,
        moneyReceiptNumber: toBepatchedData.moneyReceiptNumber,
        plotSize: toBepatchedData.plotSize,
        bookingDate: toBepatchedData.bookingDate,
        agreementDate: toBepatchedData.agreementDate,
        introduced: toBepatchedData.introduced,
        verifiedBy: toBepatchedData.verifiedBy,
      }
    )
    this.patchInstallmentSection(toBepatchedData.installments);
  }


  patchInstallmentSection(installmentArray){
    if(installmentArray){
      for(let data of installmentArray){

        let date: FormControl = new FormControl('');
        let amount: FormControl = new FormControl('');
        let modeOfPayment: FormControl = new FormControl('');
        let chequeNo: FormControl = new FormControl('');
        let id: FormControl = new FormControl('');

        date.setValue(data.date);
        amount.setValue(data.amount);
        modeOfPayment.setValue(data.modeOfPayment);
        chequeNo.setValue(data.chequeNo);
        id.setValue(data.id);

        this.installmentsList.push(new FormGroup({
          date: date,
          amount: amount,
          modeOfPayment: modeOfPayment,
          chequeNo:chequeNo,
          id:id        
        }));
      }
    }
  }

  get installmentsList() {
    return this.bookingForm.get('installmentsList') as FormArray;
  }

  addInstallmentGroup() {
    this.installmentsList.push(new FormGroup({
      date: new FormControl(''),
      amount: new FormControl(''),
      modeOfPayment: new FormControl(''),
      chequeNo:new FormControl(''),
      id: new FormControl('')
    }));
  }

  removeInstallmentGroup(index: number) {
    this.installmentsList.removeAt(index);
  }

}
