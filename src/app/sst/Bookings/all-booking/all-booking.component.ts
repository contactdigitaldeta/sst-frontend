import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BookingService } from 'src/app/Services/Booking/booking.service';

@Component({
  selector: 'app-all-booking',
  templateUrl: './all-booking.component.html',
  styleUrls: ['./all-booking.component.scss']
})
export class AllBookingComponent implements OnInit , AfterViewInit{


 allbookings: Array<any>
 search;


  constructor(private bookingService: BookingService) { }

  ngAfterViewInit(){
    this.getAllBooking();
  }

  ngOnInit(): void {
      this.getAllBooking();
  }

  getAllBooking(){
    this.bookingService.getAllBooking().subscribe(data => {
      this.allbookings = data;
    })

  }


}
