import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LandOwnerService } from 'src/app/Services/LandOwner/land-owner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-land',
  templateUrl: './details-land.component.html',
  styleUrls: ['./details-land.component.scss']
})
export class DetailsLandComponent implements OnInit {


  landDetails: any;
  installmentList:any;
  brokerinstallmentdetails:any;

  constructor(private route: ActivatedRoute,private router: Router,private landService: LandOwnerService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const landId = Number(routeParams.get('id'));
    this.landService.getLandById(landId).subscribe(data => {
      this.landDetails = data;
     this.installmentList =this.landDetails.landinstallments;
     this.brokerinstallmentdetails=this.landDetails.brokerinstallments;

    });
  }

  deleteLands() {
    this.landService.deleteLandById(this.landDetails.id).subscribe(data => {
      console.log("delete lead successfully" + this.landDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteLands();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.landDetails.name} Land has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-lands']);
      }
    });
  }

}
