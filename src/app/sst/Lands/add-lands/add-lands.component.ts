import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LandOwnerService } from 'src/app/Services/LandOwner/land-owner.service';
import Swal from 'sweetalert2';


interface Lead {
  viewValue: string;
}

@Component({
  selector: 'app-add-lands',
  templateUrl: './add-lands.component.html',
  styleUrls: ['./add-lands.component.scss']
})
export class AddLandsComponent implements OnInit {


  isUpdate: boolean = false;
  customerData: any;
  userId: any;
  id: string;
  landDetailsToPatch;
  buttonVal;
  headerVal;
  isPatching = false;

  dists: Lead[] = [
    { viewValue: 'CUTTACK'},
    { viewValue: 'KHORDHA'},
    { viewValue: 'PURI'},
  ];

  installmentSection = new FormGroup({
    date: new FormControl(''),
    amount: new FormControl(''),
    modeOfPayment: new FormControl(''),
    chequeNo:new FormControl(''), 
    id: new FormControl('')
  })
  brokersection=new FormGroup({
    date: new FormControl(''),
    amount: new FormControl(''),
    modeOfPayment: new FormControl(''),
    chequeNo:new FormControl(''), 
    id: new FormControl('')
  })
  landForm: FormGroup;
  selectDistricts = new FormControl('');

  constructor(private landService: LandOwnerService,private route: ActivatedRoute,private router: Router) {

    this.landForm = new FormGroup({
      name: new FormControl(''),
      fathersName: new FormControl(''),
      permanentAddress: new FormControl(''),
      postalCode: new FormControl(''),
      correspondenceAddress: new FormControl(''),
      // mouza: new FormControl(''),
      area: new FormControl(''),
      // landCost: new FormControl(''),
      emailId: new FormControl(''),
      mobileNumber: new FormControl(''),
      alternateMobileNumber: new FormControl('',),
      dateOfBirth: new FormControl(''),
      occupation: new FormControl(''),
      landPlotNumber: new FormControl(''),
      khataNumber: new FormControl(''),
      landTotalCost: new FormControl(''),
      monthlyScheme: new FormControl(''),
      advanceRupees: new FormControl(''),
      // plotSize: new FormControl(''),
      introduced: new FormControl(''),
      verify: new FormControl(''),
      selectDistricts: this.selectDistricts,
      selectTahasil: new FormControl(''),
      selectVillage: new FormControl(''),
      riCircle: new FormControl(''),
      khatiyanNumber: new FormControl(''),
      brokerName: new FormControl(''),
      bookingDate: new FormControl(''),
      agreementDate: new FormControl(''),
      brokerPhone: new FormControl(''),
      brokerAddress: new FormControl(''),
      totalBrokerCommission: new FormControl(''),
      advancePayment: new FormControl(''),
      dueAmount: new FormControl(''),
      dueDate: new FormControl(''),
      landinstallmentslist: new FormArray([]),
      brokerinstallmentslist:new FormArray([])

    });

   }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const landId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Land id " + landId);
    if (landId != 0) {
      this.headerVal = "Update";
      this.isUpdate = true;
      this.landService.getLandById(landId).subscribe(data => {
        this.landDetailsToPatch = data;
        console.log('individual land details patching' + JSON.stringify(this.landDetailsToPatch));
        this.patchLandDetails(this.landDetailsToPatch);
      })
      this.isPatching = true;

    }
  }

  addLands() {
    console.log('land data ' + JSON.stringify(this.landForm.value));
    this.landService.addLand(this.landForm.value).subscribe(data => {
      console.log(data);
    });
  }


  saveLands() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.addLands();
            Swal.fire({
              title: 'Submited',
              text: `Land form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-lands']);
          }
        });
  }


  updateLands() {
    console.log('Update land data '+ JSON.stringify(this.landForm.value));
    this.landService.updateLand(this.landForm.value, this.landDetailsToPatch.id).subscribe(data => {
      console.log("updated land return value "+ JSON.stringify(data));
    })
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateLands();
            Swal.fire({
              title: 'Updated',
              text: `Land has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-lands']);
          }
        });
  }




  patchLandDetails(toBepatchedData) {

    this.landForm.patchValue(
      {
        name: toBepatchedData.name,
        fathersName: toBepatchedData.fathersName,
        permanentAddress: toBepatchedData.permanentAddress,
        postalCode: toBepatchedData.postalCode,
        correspondenceAddress: toBepatchedData.correspondenceAddress,
        area: toBepatchedData.area,
        emailId: toBepatchedData.emailId,
        mobileNumber: toBepatchedData.mobileNumber,
        alternateMobileNumber: toBepatchedData.alternateMobileNumber,
        dateOfBirth: toBepatchedData.dateOfBirth,
        occupation: toBepatchedData.occupation,
        landPlotNumber: toBepatchedData.landPlotNumber,
        khataNumber: toBepatchedData.khataNumber,
        landTotalCost: toBepatchedData.landTotalCost,
        monthlyScheme: toBepatchedData.monthlyScheme,
        advanceRupees: toBepatchedData.advanceRupees,
        introduced: toBepatchedData.introduced,
        verify: toBepatchedData.verify,
        selectDistricts: toBepatchedData.selectDistricts,
        selectTahasil: toBepatchedData.selectTahasil,
        selectVillage: toBepatchedData.selectVillage,
        riCircle: toBepatchedData.riCircle,
        khatiyanNumber: toBepatchedData.khatiyanNumber,
        brokerName: toBepatchedData.brokerName,
        bookingDate: toBepatchedData.bookingDate,
        agreementDate: toBepatchedData.agreementDate,
        brokerPhone: toBepatchedData.brokerPhone,
        brokerAddress: toBepatchedData.brokerAddress,
        totalBrokerCommission: toBepatchedData.totalBrokerCommission,
        advancePayment: toBepatchedData.advancePayment,
        dueAmount: toBepatchedData.dueAmount,
        dueDate: toBepatchedData.dueDate

      }


    )
    this.patchInstallmentSection(toBepatchedData.landinstallments);
    this.patchCommisionSection(toBepatchedData.brokerinstallments);

  }


  patchInstallmentSection(installmentArray){
    if(installmentArray){
      for(let data of installmentArray){

        let date: FormControl = new FormControl('');
        let amount: FormControl = new FormControl('');
        let modeOfPayment: FormControl = new FormControl('');
        let chequeNo: FormControl = new FormControl('');
        let id: FormControl = new FormControl('');

        date.setValue(data.date);
        amount.setValue(data.amount);
        modeOfPayment.setValue(data.modeOfPayment);
        chequeNo.setValue(data.chequeNo);
        id.setValue(data.id);

        

        this.landinstallmentslist.push(new FormGroup({
          date: date,
          amount: amount,
          modeOfPayment: modeOfPayment,
          chequeNo:chequeNo,
          id:id        
        }));
      }
    }
  }


  patchCommisionSection(installmentArray){
    for(let data of installmentArray){

      let date: FormControl = new FormControl('');
      let amount: FormControl = new FormControl('');
      let modeOfPayment: FormControl = new FormControl('');
      let chequeNo: FormControl = new FormControl('');
      let id: FormControl = new FormControl('');

      date.setValue(data.date);
      amount.setValue(data.amount);
      modeOfPayment.setValue(data.modeOfPayment);
      chequeNo.setValue(data.chequeNo);
      id.setValue(data.id);

      this.brokerinstallmentslist.push (new FormGroup({
        date: date,
        amount: amount,
        modeOfPayment: modeOfPayment,
        chequeNo:chequeNo,
        id:id        
      }));
    }



  }

  get landinstallmentslist() {
    return this.landForm.get('landinstallmentslist') as FormArray;
  }
  get brokerinstallmentslist(){

    return this.landForm.get('brokerinstallmentslist') as FormArray;
  }

  addInstallmentGroup() {
    this.landinstallmentslist.push(this.installmentSection);
  }
  addCommisionGroup(){
    this.brokerinstallmentslist.push(this.brokersection);
  }

  removeInstallmentGroup(index: number) {
    this.landinstallmentslist.removeAt(index);
  }
  removeBrokerGroup(index:number){
    this.brokerinstallmentslist.removeAt(index);
  }
}
