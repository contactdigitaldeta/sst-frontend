import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllLandsComponent } from './all-lands.component';

describe('AllLandsComponent', () => {
  let component: AllLandsComponent;
  let fixture: ComponentFixture<AllLandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllLandsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllLandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
