import { AfterViewInit, Component, OnInit } from '@angular/core';
import { LandOwnerService } from 'src/app/Services/LandOwner/land-owner.service';

@Component({
  selector: 'app-all-lands',
  templateUrl: './all-lands.component.html',
  styleUrls: ['./all-lands.component.scss']
})
export class AllLandsComponent implements OnInit , AfterViewInit{


  alllands: Array<any>
  search;


  constructor(private landService: LandOwnerService ) { }

  ngAfterViewInit(){
    this.getAllLands();
  }

  ngOnInit(): void {
      this.getAllLands();
  }

  getAllLands(){
    this.landService.getAllLands().subscribe(data => {
      this.alllands = data;
    });
  }

}
