import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/Services/Users/users.service';

@Component({
  selector: 'app-all-user',
  templateUrl: './all-user.component.html',
  styleUrls: ['./all-user.component.scss']
})
export class AllUserComponent implements OnInit {

  allbrokers: any;
  search;


  constructor(private brokerServ: UsersService) { }

  ngOnInit(): void {
    this.brokerServ.getAllBroker().subscribe(data => {
      this.allbrokers = data;
      //console.log('all leads '+JSON.stringify(this.allleads));
    });
  }

}
