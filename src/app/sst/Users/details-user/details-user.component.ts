import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/Services/Users/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.scss']
})
export class DetailsUserComponent implements OnInit {


  brokerDetails: any;

  constructor(private route: ActivatedRoute,private router: Router,private brokerService: UsersService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const brokerId = Number(routeParams.get('id'));
    this.brokerService.getBrokerById(brokerId).subscribe(data => {
      this.brokerDetails = data;
    });
  }


  deleteBrokers() {
    this.brokerService.deleteBrokerById(this.brokerDetails.id).subscribe(data => {
      console.log("delete deleted successfully" + this.brokerDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteBrokers();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.brokerDetails.name} Broker has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-user']);
      }
    });
  }

}
