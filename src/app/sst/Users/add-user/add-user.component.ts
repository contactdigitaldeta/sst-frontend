import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/Services/Users/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  isUpdate: boolean = false;
  userId: any;
  id: string;
  brokerDetailsToPatch;
  buttonVal;
  headerVal;


  brokerForm = new FormGroup({
    name: new FormControl(''),
    fathersName: new FormControl(''),
    permanentAddress: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),
    emailId: new FormControl(''),
    mobileNumber: new FormControl(''),
    alternateMobileNumber: new FormControl(''),
  });

  constructor(private brokerServ: UsersService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const brokerId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Broker id " + brokerId);
    if (brokerId != 0) {
      this.headerVal = "Update";
      this.isUpdate = true;
      this.brokerServ.getBrokerById(brokerId).subscribe(data => {
        this.brokerDetailsToPatch = data;
        console.log('individual broker details patching' + JSON.stringify(this.brokerDetailsToPatch));
        this.patchBrokerDetails(this.brokerDetailsToPatch);
      })

    }
  }

  addBrokers() {
    console.log('Broker data ' + JSON.stringify(this.brokerForm.value));
    this.brokerServ.addBroker(this.brokerForm.value).subscribe(data => {
      console.log(data);
    });
  }


  saveBroker() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.addBrokers();
            Swal.fire({
              title: 'Submited',
              text: `Broker form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-user']);
          }
        });
  }


  updateBrokers() {
    console.log('Update broker data '+ JSON.stringify(this.brokerForm.value));
    this.brokerServ.updateBroker(this.brokerForm.value, this.brokerDetailsToPatch.id).subscribe(data => {
      console.log("updated broker return value "+ JSON.stringify(data));
    });
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateBrokers();
            Swal.fire({
              title: 'Updated',
              text: `Broker has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-user']);
          }
        });
  }


  patchBrokerDetails(toBepatchedData) {

    this.brokerForm.patchValue(
      {
        name: toBepatchedData.name,
        fathersName: toBepatchedData.fathersName,
        permanentAddress: toBepatchedData.permanentAddress,
        city: toBepatchedData.city,
        state: toBepatchedData.state,
        postalCode: toBepatchedData.postalCode,
        emailId: toBepatchedData.emailId,
        mobileNumber: toBepatchedData.mobileNumber,
        alternateMobileNumber: toBepatchedData.alternateMobileNumber
      }
    )

  }


}
