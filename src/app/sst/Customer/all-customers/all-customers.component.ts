import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CustomersService } from 'src/app/Services/Customers/customers.service';

@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
  styleUrls: ['./all-customers.component.scss']
})
export class AllCustomersComponent implements OnInit , AfterViewInit{


  allcustomers: Array<any>
  search;


  constructor(private customerService: CustomersService) { }

  ngAfterViewInit(){
    this.getAllCustomers();
  }

  ngOnInit(): void {
      this.getAllCustomers();
  }
  getAllCustomers(){
    this.customerService.getAllCustomer().subscribe(data => {
      this.allcustomers = data;
    })
  }

}
