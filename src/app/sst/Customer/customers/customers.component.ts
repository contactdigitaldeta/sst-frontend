import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from 'src/app/Services/Customers/customers.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {


  isUpdate: boolean = false;
  customerData: any;
  userId: any;
  id: string;
  customerDetailsToPatch;
  buttonVal;
  headerVal;


  customerForm = new FormGroup({
    firstName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(15), Validators.pattern('^[a-zA-Z]+')]),
    middleName: new FormControl(''),
    lastName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(15), Validators.pattern('^[a-zA-Z]+')]),
    fathersName: new FormControl(''),
    permanentAddress: new FormControl(''),
    correspondenceAddress: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),
    emailId: new FormControl(''),
    mobileNumber: new FormControl('',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
    alternateMobileNumber: new FormControl(''),
    dateOfBirth: new FormControl(''),
    dateOfBooking: new FormControl(''),
  });
  constructor(private customerService: CustomersService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const customerId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Customers id " + customerId);
      if (customerId != 0) {
        this.headerVal = "Update";
        this.isUpdate = true;
        this.customerService.getCustomerById(customerId).subscribe(data => {
          this.customerDetailsToPatch = data;
          console.log('individual customer details patching' + JSON.stringify(this.customerDetailsToPatch));
          this.patchCustomerDetails(this.customerDetailsToPatch);
        })

      }
  }


  addCustomers() {
    console.log('customer data ' + JSON.stringify(this.customerForm.value));
    this.customerService.addCustomer(this.customerForm.value).subscribe(data => {
      console.log(data);
    });
  }


  saveCustomer() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.addCustomers();
            Swal.fire({
              title: 'Submited',
              text: `Customer form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-customers']);
          }
        });
  }


  updateCustomers() {
    console.log('Update customer data '+ JSON.stringify(this.customerForm.value));
    this.customerService.updateCustomer(this.customerForm.value, this.customerDetailsToPatch.id).subscribe(data => {
      console.log("updated customer return value "+ JSON.stringify(data));
    })
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateCustomers();
            Swal.fire({
              title: 'Updated',
              text: `Customer has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-customers']);
          }
        });
  }


  patchCustomerDetails(toBepatchedData) {

    this.customerForm.patchValue(
      {
        firstName: toBepatchedData.firstName,
        middleName: toBepatchedData.middleName,
        lastName: toBepatchedData.lastName,
        fathersName: toBepatchedData.fathersName,
        permanentAddress: toBepatchedData.permanentAddress,
        correspondenceAddress: toBepatchedData.correspondenceAddress,
        city: toBepatchedData.city,
        state: toBepatchedData.state,
        postalCode: toBepatchedData.postalCode,
        emailId: toBepatchedData.emailId,
        mobileNumber: toBepatchedData.mobileNumber,
        alternateMobileNumber: toBepatchedData.alternateMobileNumber,
        dateOfBirth: toBepatchedData.dateOfBirth,
        dateOfBooking: toBepatchedData.dateOfBooking,
      }
    )

  }

}
