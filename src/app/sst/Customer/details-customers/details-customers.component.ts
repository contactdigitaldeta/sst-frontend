import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from 'src/app/Services/Customers/customers.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-customers',
  templateUrl: './details-customers.component.html',
  styleUrls: ['./details-customers.component.scss']
})
export class DetailsCustomersComponent implements OnInit {


  customerDetails: any;

  constructor(private route: ActivatedRoute,private router: Router,private customerService: CustomersService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const customerId = Number(routeParams.get('id'));
    this.customerService.getCustomerById(customerId).subscribe(data => {
      this.customerDetails = data;
    });
  }

  deleteCustomers() {
    this.customerService.deleteCustomerById(this.customerDetails.id).subscribe(data => {
      console.log("delete deleted successfully" + this.customerDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteCustomers();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.customerDetails.firstName} Customer has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-customers']);
      }
    });
  }

}
