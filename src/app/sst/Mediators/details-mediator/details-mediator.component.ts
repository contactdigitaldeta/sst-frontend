import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MediatorService } from 'src/app/Services/Mediator/mediator.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-mediator',
  templateUrl: './details-mediator.component.html',
  styleUrls: ['./details-mediator.component.scss']
})
export class DetailsMediatorComponent implements OnInit {


  mediatorDetails: any;

  constructor(private route: ActivatedRoute,private router: Router,private mediatorService: MediatorService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const mediatorId = Number(routeParams.get('id'));
    this.mediatorService.getMediatorById(mediatorId).subscribe(data => {
      this.mediatorDetails = data;
    });
  }

  deleteMediators() {
    this.mediatorService.deleteMediatorById(this.mediatorDetails.id).subscribe(data => {
      console.log("delete mediator successfully" + this.mediatorDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteMediators();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.mediatorDetails.name} Mediator has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-mediator']);
      }
    });
  }

}
