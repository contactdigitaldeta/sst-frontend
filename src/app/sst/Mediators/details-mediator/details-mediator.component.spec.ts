import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsMediatorComponent } from './details-mediator.component';

describe('DetailsMediatorComponent', () => {
  let component: DetailsMediatorComponent;
  let fixture: ComponentFixture<DetailsMediatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsMediatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsMediatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
