import { Component, OnInit } from '@angular/core';
import { MediatorService } from 'src/app/Services/Mediator/mediator.service';

@Component({
  selector: 'app-all-mediator',
  templateUrl: './all-mediator.component.html',
  styleUrls: ['./all-mediator.component.scss']
})
export class AllMediatorComponent implements OnInit {


  allmediators: any;
  search;

  constructor(private mediatorService: MediatorService) { }

  ngOnInit(): void {
    this.mediatorService.getAllMediator().subscribe(data => {
      this.allmediators = data;
      //console.log('all leads '+JSON.stringify(this.allleads));
    });
  }

}
