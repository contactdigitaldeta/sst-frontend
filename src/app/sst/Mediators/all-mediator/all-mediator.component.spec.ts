import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMediatorComponent } from './all-mediator.component';

describe('AllMediatorComponent', () => {
  let component: AllMediatorComponent;
  let fixture: ComponentFixture<AllMediatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMediatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMediatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
