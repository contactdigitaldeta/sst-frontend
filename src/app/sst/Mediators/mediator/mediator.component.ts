import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MediatorService } from 'src/app/Services/Mediator/mediator.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mediator',
  templateUrl: './mediator.component.html',
  styleUrls: ['./mediator.component.scss']
})
export class MediatorComponent implements OnInit {


  isUpdate: boolean = false;
  userId: any;
  id: string;
  mediatorDetailsToPatch;
  buttonVal;
  headerVal;

  mediatorForm = new FormGroup({
    name: new FormControl(''),
    fathersName: new FormControl(''),
    permanentAddress: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),
    emailId: new FormControl(''),
    mobileNumber: new FormControl(''),
    alternateMobileNumber: new FormControl(''),
  });
  
  constructor(private route: ActivatedRoute,private router: Router,private mediatorServ: MediatorService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const mediatorId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Mediator id " + mediatorId);
    if (mediatorId != 0) {
      this.headerVal = "Update";
      this.isUpdate = true;
      this.mediatorServ.getMediatorById(mediatorId).subscribe(data => {
        this.mediatorDetailsToPatch = data;
        console.log('individual mediator details patching' + JSON.stringify(this.mediatorDetailsToPatch));
        this.patchMediatorDetails(this.mediatorDetailsToPatch);
      })

    }
  }


  addMediators() {
    console.log('Mediator data ' + JSON.stringify(this.mediatorForm.value));
    this.mediatorServ.addMediator(this.mediatorForm.value).subscribe(data => {
      console.log(data);
    });
  }


  saveMediator() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.addMediators();
            Swal.fire({
              title: 'Submited',
              text: `Mediator form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-mediator']);
          }
        });
  }


  updateMediators() {
    console.log('Update mediator data '+ JSON.stringify(this.mediatorForm.value));
    this.mediatorServ.updateMediator(this.mediatorForm.value, this.mediatorDetailsToPatch.id).subscribe(data => {
      console.log("updated mediator return value "+ JSON.stringify(data));
    });
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateMediators();
            Swal.fire({
              title: 'Updated',
              text: `Mediator has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-mediator']);
          }
        });
  }



  patchMediatorDetails(toBepatchedData) {

    this.mediatorForm.patchValue(
      {
        name: toBepatchedData.name,
        fathersName: toBepatchedData.fathersName,
        permanentAddress: toBepatchedData.permanentAddress,
        city: toBepatchedData.city,
        state: toBepatchedData.state,
        postalCode: toBepatchedData.postalCode,
        emailId: toBepatchedData.emailId,
        mobileNumber: toBepatchedData.mobileNumber,
        alternateMobileNumber: toBepatchedData.alternateMobileNumber
      }
    )

  }

}
