import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCourtexpensesComponent } from './all-courtexpenses.component';

describe('AllCourtexpensesComponent', () => {
  let component: AllCourtexpensesComponent;
  let fixture: ComponentFixture<AllCourtexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllCourtexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCourtexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
