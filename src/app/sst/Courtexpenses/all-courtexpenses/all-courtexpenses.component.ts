import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CourtexpensesService } from 'src/app/Services/Courtexpenses/courtexpenses.service';

@Component({
  selector: 'app-all-courtexpenses',
  templateUrl: './all-courtexpenses.component.html',
  styleUrls: ['./all-courtexpenses.component.scss']
})
export class AllCourtexpensesComponent implements OnInit, AfterViewInit{

  allcourtexpenses:any;
  search;

  constructor(private courtservice:CourtexpensesService) { }


  ngAfterViewInit(){
    this.getAllCourtExpnese();
  }
  
  ngOnInit(): void {
      this.getAllCourtExpnese();
  }

  getAllCourtExpnese(){
    this.courtservice.getAllCourtExpenses().subscribe(data => {
      this.allcourtexpenses= data;
    })
  }

}
