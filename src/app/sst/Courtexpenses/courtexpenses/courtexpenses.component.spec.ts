import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourtexpensesComponent } from './courtexpenses.component';

describe('CourtexpensesComponent', () => {
  let component: CourtexpensesComponent;
  let fixture: ComponentFixture<CourtexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourtexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourtexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
