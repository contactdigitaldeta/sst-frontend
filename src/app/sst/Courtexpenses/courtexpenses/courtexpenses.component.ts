import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CourtexpensesService } from 'src/app/Services/Courtexpenses/courtexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-courtexpenses',
  templateUrl: './courtexpenses.component.html',
  styleUrls: ['./courtexpenses.component.scss']
})
export class CourtexpensesComponent implements OnInit {

  isUpdate: boolean = false;
  courtDetailsToPatch: any;

  constructor(private courtservice:CourtexpensesService,private route: ActivatedRoute,private router: Router) { }


  courtForm = new FormGroup({
    personName:new FormControl(''),
    dateOfPayment:new FormControl(''),
    amount:new FormControl(''),
    moodOfPayment:new FormControl(''),
    purposeOfPayment:new FormControl(''),
    referenceNo:new FormControl('')
  });

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const courtId = Number(routeParams.get('id'));
    this.courtservice.getAllCourtExpensesById(courtId).subscribe(data=>{
      this.courtDetailsToPatch=data;
      this.patchCourtDetails(this.courtDetailsToPatch);
      this.isUpdate= true;
    })
  }

  savecourtexpense(){
    console.log(JSON.stringify(this.courtForm.value));
    this.courtservice.addcourtexpense(this.courtForm.value).subscribe(data=>{
      console.log(data);
    })

  }


  saveExpenses(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, submit it!'
    }).then(result => {
      if (result.value) {
        this.savecourtexpense();
        Swal.fire({
          title: 'Submited',
          text: `Courtform has been Submitted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-courtexpense']);
      }
    });

  }

  // upadte service call starts here

  updateCourtExpenses(){
    this.courtservice.updatecourtexpenses(this.courtForm.value,this.courtDetailsToPatch.id).subscribe( data=>{
      console.log("updated office expenses return value "+ JSON.stringify(data));
    })
  }

  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateCourtExpenses();
            Swal.fire({
              title: 'Updated',
              text: `Details has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['all-courtexpense']);
          }
        });
  }


  
  patchCourtDetails(toBepatchedData){
    this.courtForm.patchValue({
        personName:toBepatchedData.personName,
        dateOfPayment:toBepatchedData.dateOfPayment,
        amount:toBepatchedData.amount,
        moodOfPayment:toBepatchedData.moodOfPayment,
        purposeOfPayment:toBepatchedData.purposeOfPayment,
        referenceNo:toBepatchedData.referenceNo,

    })


  }



}
