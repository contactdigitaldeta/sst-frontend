import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCourtexpensesComponent } from './details-courtexpenses.component';

describe('DetailsCourtexpensesComponent', () => {
  let component: DetailsCourtexpensesComponent;
  let fixture: ComponentFixture<DetailsCourtexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsCourtexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCourtexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
