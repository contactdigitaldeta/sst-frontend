import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourtexpensesService } from 'src/app/Services/Courtexpenses/courtexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-courtexpenses',
  templateUrl: './details-courtexpenses.component.html',
  styleUrls: ['./details-courtexpenses.component.scss']
})
export class DetailsCourtexpensesComponent implements OnInit {

  courtDetails:any;
  constructor(private courtservice:CourtexpensesService,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const courtExpenceId = Number(routeParams.get('id'));
    this.courtservice.getAllCourtExpensesById(courtExpenceId).subscribe(data => {
      this.courtDetails=data;
      console.log(data);
    })
  }

  deletecourtexpenses(){
    this.courtservice.deleteCourtexpenses(this.courtDetails.id).subscribe(data => {
      console.log("CourtExpenses deleted succefully" + this.courtDetails.id);

    })
  }
  plzconfirm(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deletecourtexpenses();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.courtDetails.id} CourtExpenses has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-courtexpense']);
      }
    });
  }




}
