import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectexpensesComponent } from './projectexpenses.component';

describe('ProjectexpensesComponent', () => {
  let component: ProjectexpensesComponent;
  let fixture: ComponentFixture<ProjectexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
