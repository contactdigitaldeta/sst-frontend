import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectexpensesService } from 'src/app/Services/Projectexpenses/projectexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-projectexpenses',
  templateUrl: './projectexpenses.component.html',
  styleUrls: ['./projectexpenses.component.scss']
})
export class ProjectexpensesComponent implements OnInit {

  customerDetailsToPatch;
  isUpdate: boolean = false;
  isPatching = false;
  id: string;
  


  installmentSection = new FormGroup({
    date: new FormControl(''),
    amount: new FormControl(''),
    modeOfPayment: new FormControl(''),
    referenceNo:new FormControl(''),
    purposeOfPayment:new FormControl(''),
    id: new FormControl('')
  })

  constructor(private projectexpenses:ProjectexpensesService,private route: ActivatedRoute,private router: Router) { }

  projectexpensesForm = new FormGroup({
    projectName: new FormControl(''),
    dateOfStart: new FormControl(''),
    location: new FormControl(''),
    pexpenseslotslist : new FormArray([])
  });
  ngOnInit(): void {

    const routeParams = this.route.snapshot.paramMap;
    const projectId = Number(routeParams.get('id'));
    if (projectId != 0) {
      // this.headerVal = "Update";
      this.isUpdate = true;
      this.projectexpenses.getProjectExpensesById(projectId).subscribe(data => {
        this.customerDetailsToPatch = data;
        console.log('individual customer details patching' + JSON.stringify(this.customerDetailsToPatch));
        this.patchCustomerDetails(this.customerDetailsToPatch);
        this.isPatching = true;
      })

    }

  }

  //add project expenses service call
  createprojectexpenses(){
    debugger
    console.log('booking data ' + JSON.stringify(this.projectexpensesForm.value));
    this.projectexpenses.addProjectExpenses(this.projectexpensesForm.value).subscribe(data =>{
      console.log(data);
    })
  }


  saveProjectExpenses() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.createprojectexpenses();
            Swal.fire({
              title: 'Submited',
              text: `Expenses form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-projectexpenses']);
          }
        });
  }

  //update service call starts from here and


  updateProjectExpenses() {
    console.log('Update Projectexpenses data '+ JSON.stringify(this.projectexpensesForm.value));
    this.projectexpenses.updateProjectExpenses(this.projectexpensesForm.value, this.customerDetailsToPatch.id).subscribe(data => {
      console.log("updated booking return value "+ JSON.stringify(data));
    })
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateProjectExpenses();
            Swal.fire({
              title: 'Updated',
              text: `Booking has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-projectexpenses']);
          }
        });
  }



  patchCustomerDetails(toBepatchedData){
    this.projectexpensesForm.patchValue({
      projectName:toBepatchedData.projectName,
      dateOfStart: toBepatchedData.dateOfStart,
      location: toBepatchedData.location,
    })
    this.patchInstallmentSection(toBepatchedData.pexpenseslots)
    

  }



  patchInstallmentSection(installmentArray){
    if(installmentArray){
      for(let data of installmentArray){

        let dateOfPayment: FormControl = new FormControl('');
        let amount: FormControl = new FormControl('');
        let modeOfPayment: FormControl = new FormControl('');
        let referenceNo: FormControl = new FormControl('');
        let purposeOfPayment: FormControl = new FormControl('');
        let id: FormControl = new FormControl('');

        dateOfPayment.setValue(data.dateOfPayment);
        amount.setValue(data.amount);
        modeOfPayment.setValue(data.modeOfPayment);
        referenceNo.setValue(data.referenceNo);
        purposeOfPayment.setValue(data.purposeOfPayment);
        id.setValue(data.id);

        this.pexpenseslotslist.push(new FormGroup({
          dateOfPayment: dateOfPayment,
          amount: amount,
          modeOfPayment: modeOfPayment,
          referenceNo:referenceNo,
          purposeOfPayment: purposeOfPayment,
          id:id        
        }));
      }
    }
  }

  




  get pexpenseslotslist() {
    return this.projectexpensesForm.get('pexpenseslotslist') as FormArray;
  }

  addInstallmentGroup() {
    this.pexpenseslotslist.push(new FormGroup({
      dateOfPayment: new FormControl(''),
      amount: new FormControl(''),
      modeOfPayment: new FormControl(''),
      referenceNo:new FormControl(''),
      purposeOfPayment:new FormControl(''),
      id: new FormControl(''),
    }));
  }

  removeInstallmentGroup(index: number) {
    this.pexpenseslotslist.removeAt(index);
  }

}
