import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectexpensesService } from 'src/app/Services/Projectexpenses/projectexpenses.service';

@Component({
  selector: 'app-all-projectexpenses',
  templateUrl: './all-projectexpenses.component.html',
  styleUrls: ['./all-projectexpenses.component.scss']
})
export class AllProjectexpensesComponent implements OnInit , AfterViewInit{

  allprojectexpenses:Array<any>;
  search;
  projectId:any;

  constructor(private projectservice: ProjectexpensesService, private router:Router,private route: ActivatedRoute) { }



  ngAfterViewInit(){
    this.getAllprojectexpenses();
  }

  ngOnInit(): void {
    this.getAllprojectexpenses();
  }

  getAllprojectexpenses(){
    this.projectservice.getAllProjectExpenses().subscribe(data => {
      this.allprojectexpenses=data;
      // this.projectId=this.allprojectexpenses.Id;
      const routeParams = this.route.snapshot.paramMap;
     this.projectId = Number(routeParams.get('id'));
    })

  }

  newtab(){
    const url = this.router.serializeUrl(
      this.router.createUrlTree([`/ecommerce/products/${this.projectId}`])
    );
    // this.url=this.router.navigate(['/ecommerce/products/',this.projectId]);
    window.open(url,'_blank');
  }

}


// ['/detailsprojectexpenses/',projectexpense.id]
