import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProjectexpensesComponent } from './all-projectexpenses.component';

describe('AllProjectexpensesComponent', () => {
  let component: AllProjectexpensesComponent;
  let fixture: ComponentFixture<AllProjectexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllProjectexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProjectexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
