import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectexpensesService } from 'src/app/Services/Projectexpenses/projectexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-projectexpenses',
  templateUrl: './details-projectexpenses.component.html',
  styleUrls: ['./details-projectexpenses.component.scss']
})
export class DetailsProjectexpensesComponent implements OnInit {

  detailsprojectexpense: any;
  slotdetails:any;

  constructor(private projectexpenseservice:ProjectexpensesService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const projectId = Number(routeParams.get('id'));
    this.projectexpenseservice.getProjectExpensesById(projectId).subscribe(data => {
      this.detailsprojectexpense=data;
      this.slotdetails=this.detailsprojectexpense.pexpenseslots;
    })
  }


  //delete service calle

  deleteProjectexpense(){
    this.projectexpenseservice.deleteProjectExpensesById(this.detailsprojectexpense.id).subscribe(data => {
      console.log("project deleted successfully");
    })

  }
  plzconfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteProjectexpense();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.detailsprojectexpense.projectName} Expenses has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-projectexpenses']);
      }
    });

}
}
