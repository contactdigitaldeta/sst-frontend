import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsProjectexpensesComponent } from './details-projectexpenses.component';

describe('DetailsProjectexpensesComponent', () => {
  let component: DetailsProjectexpensesComponent;
  let fixture: ComponentFixture<DetailsProjectexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsProjectexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsProjectexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
