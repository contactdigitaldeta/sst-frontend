import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute ,Router} from '@angular/router';
import { SalaryexpensesService } from 'src/app/Services/Salaryexpenses/salaryexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-salaryexpenses',
  templateUrl: './salaryexpenses.component.html',
  styleUrls: ['./salaryexpenses.component.scss']
})
export class SalaryexpensesComponent implements OnInit {


  salaryDetailsToPatch;
  isUpdate: boolean = false;
  constructor(private salaryservices:SalaryexpensesService,private route: ActivatedRoute,private router: Router) { }


  salaryForm = new FormGroup({
    personName:new FormControl(''),
    dateOfPayment:new FormControl(''),
    amount:new FormControl(''),
    moodOfPayment:new FormControl(''),
    purposeOfPayment:new FormControl(''),
    referenceNo:new FormControl('')
  });

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const salaryId = Number(routeParams.get('id'));
    this.salaryservices.getAllSalaryExpensesById(salaryId).subscribe(data=>{
      this.salaryDetailsToPatch=data;
      this.patchSalaryDetails(this.salaryDetailsToPatch);
      this.isUpdate= true;
    })
  }


  savesalaryexpense(){
    console.log('booking data ' + JSON.stringify(this.salaryForm.value));
      this.salaryservices.addsalaryexpense(this.salaryForm.value).subscribe(data =>{
        console.log(data);
      })
      this.router.navigate(['/all-salaryexpenses']);
  }

  saveExpenses(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, submit it!'
    }).then(result => {
      console.log(result);
      if (result.value) {
        this.savesalaryexpense();
        Swal.fire({
          title: 'Submited',
          text: `Booking form has been Submitted.`,
          icon: 'success',
          timer: 1500
        });
        //this.router.navigate(['/all-salaryexpenses']);
      }
    });

  }

  updateSalaryexpenses(){
    this.salaryservices.updatesalaryexpenses(this.salaryForm.value,this.salaryDetailsToPatch.id).subscribe( data=>{
      console.log("updated salaryexpenses return value "+ JSON.stringify(data));
    })
  }

  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateSalaryexpenses();
            Swal.fire({
              title: 'Updated',
              text: `Details has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-salaryexpenses']);
          }
        });
  }

  // patching updated data here

  patchSalaryDetails(toBepatchedData){
    this.salaryForm.patchValue(
      {
        personName:toBepatchedData.personName,
        dateOfPayment:toBepatchedData.dateOfPayment,
        amount:toBepatchedData.amount,
        moodOfPayment:toBepatchedData.moodOfPayment,
        purposeOfPayment:toBepatchedData.purposeOfPayment,
        referenceNo:toBepatchedData.referenceNo,
      }
    )

  }

}
