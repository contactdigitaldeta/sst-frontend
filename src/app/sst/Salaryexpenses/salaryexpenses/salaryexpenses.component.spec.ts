import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryexpensesComponent } from './salaryexpenses.component';

describe('SalaryexpensesComponent', () => {
  let component: SalaryexpensesComponent;
  let fixture: ComponentFixture<SalaryexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalaryexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
