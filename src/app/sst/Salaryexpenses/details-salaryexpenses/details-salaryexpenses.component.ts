import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalaryexpensesService } from 'src/app/Services/Salaryexpenses/salaryexpenses.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-details-salaryexpenses',
  templateUrl: './details-salaryexpenses.component.html',
  styleUrls: ['./details-salaryexpenses.component.scss']
})
export class DetailsSalaryexpensesComponent implements OnInit {

  detailssalaryexpenses:any;

  constructor(private salaryservice:SalaryexpensesService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const SalaryExpenceId = Number(routeParams.get('id'));
    this.salaryservice.getAllSalaryExpensesById(SalaryExpenceId).subscribe(data=> {
      console.log(JSON.stringify(data));
      this.detailssalaryexpenses=data; 
    })

  }


  deleteSalaryexpenses(){
    this.salaryservice.deleteSalaryexpenses(this.detailssalaryexpenses.id).subscribe(data=> {
      console.log("booking deleted successfully" + this.detailssalaryexpenses.id);
    })

  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteSalaryexpenses();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.detailssalaryexpenses.personName} Expenses has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-salaryexpenses']);
      }
    });
  }

}
