import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsSalaryexpensesComponent } from './details-salaryexpenses.component';

describe('DetailsSalaryexpensesComponent', () => {
  let component: DetailsSalaryexpensesComponent;
  let fixture: ComponentFixture<DetailsSalaryexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsSalaryexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsSalaryexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
