import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SalaryexpensesService } from 'src/app/Services/Salaryexpenses/salaryexpenses.service';

@Component({
  selector: 'app-all-salaryexpenses',
  templateUrl: './all-salaryexpenses.component.html',
  styleUrls: ['./all-salaryexpenses.component.scss']
})
export class AllSalaryexpensesComponent implements OnInit, AfterViewInit {

  allsalaryexpense: Array<any>
  search;


  constructor(private salaryexpensesservice:SalaryexpensesService) { 
  }

  ngAfterViewInit(): void {
    this.getSalaryDetails();
  }
 
  ngOnInit(): void {
    this.getSalaryDetails();
  }

  getSalaryDetails(){
    this.salaryexpensesservice.getAllSalaryExpenses().subscribe(data=>{
      console.log("all sal data "+JSON.stringify(data));
      this.allsalaryexpense=data;
    })
  }

}
