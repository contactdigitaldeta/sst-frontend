import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSalaryexpensesComponent } from './all-salaryexpenses.component';

describe('AllSalaryexpensesComponent', () => {
  let component: AllSalaryexpensesComponent;
  let fixture: ComponentFixture<AllSalaryexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllSalaryexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSalaryexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
