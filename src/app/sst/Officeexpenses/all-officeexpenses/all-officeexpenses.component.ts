import { ThrowStmt } from '@angular/compiler';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { OfficeexpensesService } from 'src/app/Services/Officeexpenses/officeexpenses.service';

@Component({
  selector: 'app-all-officeexpenses',
  templateUrl: './all-officeexpenses.component.html',
  styleUrls: ['./all-officeexpenses.component.scss']
})
export class AllOfficeexpensesComponent implements OnInit, AfterViewInit{

  allofficeexpenses:Array<any>
  search;

  constructor(private officeservice:OfficeexpensesService) { }


  ngAfterViewInit(){
    this.getAllOfficeExepneses();
  }

  ngOnInit(): void {
    this.getAllOfficeExepneses();
  }

  getAllOfficeExepneses(){
    this.officeservice.getAllOfficeExpenses().subscribe(data=>{
      this.allofficeexpenses=data;
    })
    
  }

}
