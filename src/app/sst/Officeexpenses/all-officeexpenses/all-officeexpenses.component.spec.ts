import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOfficeexpensesComponent } from './all-officeexpenses.component';

describe('AllOfficeexpensesComponent', () => {
  let component: AllOfficeexpensesComponent;
  let fixture: ComponentFixture<AllOfficeexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOfficeexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllOfficeexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
