import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OfficeexpensesService } from 'src/app/Services/Officeexpenses/officeexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-officeexpenses',
  templateUrl: './officeexpenses.component.html',
  styleUrls: ['./officeexpenses.component.scss']
})
export class OfficeexpensesComponent implements OnInit {


  isUpdate: boolean = false;
  officeDetailsToPatch: any ;


  constructor(private officeservice:OfficeexpensesService,private route: ActivatedRoute,private router: Router) { }

  officeForm = new FormGroup({
    personName:new FormControl(''),
    dateOfPayment:new FormControl(''),
    amount:new FormControl(''),
    moodOfPayment:new FormControl(''),
    purposeOfPayment:new FormControl(''),
    referenceNo:new FormControl('')
  });

  ngOnInit(): void {

    const routeParams = this.route.snapshot.paramMap;
    const officeId = Number(routeParams.get('id'));
    this.officeservice.getAllOfficeExpensesById(officeId).subscribe(data=>{
      this.officeDetailsToPatch=data;
      this.patchOfficeDetails(this.officeDetailsToPatch);
      this.isUpdate= true;
    })
  }
  saveofficeexpense(){
    this.officeservice.addofficeexpense(this.officeForm.value).subscribe(data=>{
      console.log(data);
    } )
  }


  saveExpenses(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, submit it!'
    }).then(result => {
      if (result.value) {
        this.saveofficeexpense();
        Swal.fire({
          title: 'Submited',
          text: `Officeform has been Submitted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-officeexpense']);
      }
    });

  }


  // update service call starts here
  
  updateOfficeExpenses(){
    this.officeservice.updateofficeexpenses(this.officeForm.value,this.officeDetailsToPatch.id).subscribe( data=>{
      console.log("updated office expenses return value "+ JSON.stringify(data));
    })
  }

  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateOfficeExpenses();
            Swal.fire({
              title: 'Updated',
              text: `Details has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['all-officeexpense']);
          }
        });
  }


  
  patchOfficeDetails(toBepatchedData){
    this.officeForm.patchValue({
        personName:toBepatchedData.personName,
        dateOfPayment:toBepatchedData.dateOfPayment,
        amount:toBepatchedData.amount,
        moodOfPayment:toBepatchedData.moodOfPayment,
        purposeOfPayment:toBepatchedData.purposeOfPayment,
        referenceNo:toBepatchedData.referenceNo,

    })


  }

}
