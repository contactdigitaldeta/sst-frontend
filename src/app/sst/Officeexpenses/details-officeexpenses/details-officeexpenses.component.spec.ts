import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfficeexpensesComponent } from './details-officeexpenses.component';

describe('DetailsOfficeexpensesComponent', () => {
  let component: DetailsOfficeexpensesComponent;
  let fixture: ComponentFixture<DetailsOfficeexpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsOfficeexpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfficeexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
