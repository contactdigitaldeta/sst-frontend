import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OfficeexpensesService } from 'src/app/Services/Officeexpenses/officeexpenses.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-officeexpenses',
  templateUrl: './details-officeexpenses.component.html',
  styleUrls: ['./details-officeexpenses.component.scss']
})
export class DetailsOfficeexpensesComponent implements OnInit {


  detailsofficeexpenses:any;
  constructor(private officeservice:OfficeexpensesService,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const OfficeExpenceId = Number(routeParams.get('id'));
    this.officeservice.getAllOfficeExpensesById(OfficeExpenceId).subscribe(data=>{
      this.detailsofficeexpenses=data;
      console.log(data);
    })
  }

  deleteOfficeexpenses(){
    this.officeservice.deleteOfficeexpenses(this.detailsofficeexpenses.id).subscribe(data=>{
      console.log("office expenses deleted succefully" + this.detailsofficeexpenses.id);
    })
  }


  plzconfirm(){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteOfficeexpenses();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.detailsofficeexpenses.id} officeexpenses has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-officeexpense']);
      }
    });
  }

}
