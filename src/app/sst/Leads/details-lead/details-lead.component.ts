import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadService } from 'src/app/Services/Leads/lead.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details-lead',
  templateUrl: './details-lead.component.html',
  styleUrls: ['./details-lead.component.scss']
})
export class DetailsLeadComponent implements OnInit {

  leadDetails: any;

  constructor(private route: ActivatedRoute,private leadService: LeadService,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const leadId = Number(routeParams.get('id'));
    this.leadService.getLeadById(leadId).subscribe(data => {
      this.leadDetails = data;
    });
  }

  deleteLeads() {
    this.leadService.deleteLeadById(this.leadDetails.id).subscribe(data => {
      console.log("lead deleted successfully" + this.leadDetails.id);
    });
  }

  delete() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.deleteLeads();
        Swal.fire({
          title: 'Deleted!',
          text: ` ${this.leadDetails.firstName} leads has been deleted.`,
          icon: 'success',
          timer: 1500
        });
        this.router.navigate(['/all-lead']);
      }
    });
  }

}
