import { AfterViewInit, Component, OnInit } from '@angular/core';
import { LeadService } from 'src/app/Services/Leads/lead.service';

@Component({
  selector: 'app-all-lead',
  templateUrl: './all-lead.component.html',
  styleUrls: ['./all-lead.component.scss']
})
export class AllLeadComponent implements OnInit, AfterViewInit{


  allleads;
  search;
  constructor(private leadServ: LeadService) { }


  ngAfterViewInit(){
    this.getAllLead();
  }
  ngOnInit(): void {
    this.getAllLead();  
  }

  getAllLead(){
    this.leadServ.getAllLead().subscribe(data => {
      this.allleads = data;
    });
  }

}
