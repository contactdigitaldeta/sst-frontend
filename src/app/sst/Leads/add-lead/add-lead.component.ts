import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadService } from 'src/app/Services/Leads/lead.service';
import Swal from 'sweetalert2';

interface Lead {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-lead',
  templateUrl: './add-lead.component.html',
  styleUrls: ['./add-lead.component.scss']
})
export class AddLeadComponent implements OnInit {



  isUpdate: boolean = false;
  projectData: any;
  userId: any;
  id: string;
  leadDetailsToPatch;
  buttonVal;
  headerVal;

  leads: Lead[] = [
    {value: '1', viewValue: 'Walking Lead'},
    {value: '2', viewValue: 'Reference Lead'},
    {value: '3', viewValue: 'Cold Call Lead'},
    {value: '4', viewValue: 'Social Media Lead'},
    {value: '5', viewValue: 'Sales Lead'}
  ];

  leadType = new FormControl('');

  leadForm = new FormGroup({
    firstName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(15), Validators.pattern('^[a-zA-Z]+')]),
    middleName: new FormControl(''),
    lastName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(15), Validators.pattern('^[a-zA-Z]+')]),
    PhoneNumber: new FormControl('',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
    leadType: this.leadType,
    address: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    pinCode: new FormControl(''),
    companyName: new FormControl(''),
    designation: new FormControl(''),
    source: new FormControl(''),
    assignTo: new FormControl(''),
    status: new FormControl(''),
  })
  constructor(private leadServ: LeadService,private route: ActivatedRoute,private leadService: LeadService,private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const leadId = Number(routeParams.get('id'));
    this.headerVal = "Add";
    console.log("Lead id " + leadId);
    if (leadId != 0) {
      this.headerVal = "Update";
      this.isUpdate = true;
      this.leadService.getLeadById(leadId).subscribe(data => {
        this.leadDetailsToPatch = data;
        console.log('individual lead details patching' + JSON.stringify(this.leadDetailsToPatch));
        this.patchLeadDetails(this.leadDetailsToPatch);
      })

    }
  }


  addLeads() {
    console.log('Lead data ' + JSON.stringify(this.leadForm.value));
    this.leadServ.addLead(this.leadForm.value).subscribe(data => {
      console.log(data);
    });
  }

  saveLeads() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, submit it!'
        }).then(result => {
          if (result.value) {
            this.addLeads();
            Swal.fire({
              title: 'Submited',
              text: `Lead form has been Submitted.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-lead']);
          }
        });
  }



  updateLeads() {
    console.log('Update lead data '+ JSON.stringify(this.leadForm.value));
    this.leadServ.updateLead(this.leadForm.value, this.leadDetailsToPatch.id).subscribe(data => {
      console.log("updated lead return value "+ JSON.stringify(data));
    })
  }


  plzconfirm() {
    Swal.fire({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#34c38f',
          cancelButtonColor: '#f46a6a',
          confirmButtonText: 'Yes, Update it!'
        }).then(result => {
          if (result.value) {
            this.updateLeads();
            Swal.fire({
              title: 'Updated',
              text: `Lead has been Updated.`,
              icon: 'success',
              timer: 1500
            });
            this.router.navigate(['/all-lead']);
          }
        });
  }


  patchLeadDetails(toBepatchedData) {

    this.leadForm.patchValue(
      {
        firstName: toBepatchedData.firstName,
        middleName: toBepatchedData.middleName,
        lastName: toBepatchedData.lastName,
        PhoneNumber: toBepatchedData.PhoneNumber,
        leadType: toBepatchedData.leadType,
        address: toBepatchedData.address,
        city: toBepatchedData.city,
        state: toBepatchedData.state,
        pinCode: toBepatchedData.pinCode,
        companyName: toBepatchedData.companyName,
        designation: toBepatchedData.designation,
        source: toBepatchedData.source,
        assignTo: toBepatchedData.assignTo,
        status: toBepatchedData.status,
      }
    )

  }

}
