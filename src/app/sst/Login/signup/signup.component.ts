import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginserviceService } from 'src/app/Services/LoginSignup/loginservice.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  roles = new FormControl(['']);
  
  signupForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    email:    new FormControl(''),
    roles:  this.roles,
  });

 


  constructor(private Loginservice:LoginserviceService,private router:Router) { }

  ngOnInit(): void {
  }

  signup(): void {

    this.Loginservice.register(this.signupForm.value).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        if(this.isSuccessful){
          this.router.navigate(['login']);
        }
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
}

}
