import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginserviceService } from 'src/app/Services/LoginSignup/loginservice.service';
import { TokenStorageService } from 'src/app/Services/LoginSignup/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  showPassword:Boolean = false;
  

    loginForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
    });

    isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];


  constructor(private loginservice:LoginserviceService,private Tokenservice:TokenStorageService,private router:Router) { }

  ngOnInit(): void {

    // if (this.Tokenservice.getToken()) {
    //   this.isLoggedIn = true;
    //   this.roles = this.Tokenservice.getUser().roles;
    // }
  }

  signin(){
    this.loginservice.Login(this.loginForm.value).subscribe(data => {
      this.Tokenservice.saveToken(data.accessToken);
        this.Tokenservice.saveUser(data);
      const token_key = this.Tokenservice.getToken();
      if(token_key != null){
        this.router.navigate(['/dashboard']) ;  
      } else{
        this.reloadPage();
      }
      // else{
      //   this.isLoggedIn=false;
      //   this.reloadPage();
      // }
      this.isLoginFailed = false;
      this.roles = this.Tokenservice.getUser().roles;
      // this.reloadPage();
     
    },
    err => {
      this.errorMessage = err.error.message;
      this.isLoginFailed = true;
    }
    
    );
  }

  reloadPage(): void {
    window.location.reload();
  }



     /*password hide unhide function initiate*/

  hidepass(input: any){
    input.type = input.type === 'password' ? 'text' : 'password';
    this.showPassword = false;
    //this.showPassword = !this.showPassword;
    // this.input.type = this.showPassword ? 'text' : 'password';
  }
  seepass(input: any){
    input.type = input.type === 'text' ? 'password' : 'text';
    this.showPassword = true;
  }
  

}
