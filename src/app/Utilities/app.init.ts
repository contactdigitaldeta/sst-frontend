import{KeycloakService} from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService):() => Promise<boolean> {
  return () =>
    keycloak.init({
      config: {
        // url: 'http://localhost:8080/auth',
        url: 'http://45.80.181.11:8080/auth',
         realm:'SST_LOCAL',
         clientId: 'sst_crmlocal',
       
        
        //vps serve configuration

        // url: 'http://45.80.181.11:8080/auth',
        // realm: 'SST_CRM',
        // clientId: 'sst_crm',
      },
      initOptions :{
            checkLoginIframe:true,
            checkLoginIframeInterval:25,
      },
      loadUserProfileAtStartUp:true
    });
}
