import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class LandOwnerService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(private http: HttpClient) {
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
   }

   addLand(Land): Observable<any> {
    // console.log("calling landowner");
    const url = `${this.sstUrl}/landowners`;
    return this.http.post(url, Land, httpOptions);
  }

  getAllLands(): Observable<any> {
    //console.log("calling customer by id");
    const url = `${this.sstUrl}/landowners`;
    return this.http.get(url);
  }

  getLandById(id): Observable<any> {
    //console.log("calling get customer by id");
    const url = `${this.sstUrl}/landowners/${id}`;
    return this.http.get(url, id );
  }

  updateLand(Land, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/landowners/${id}`;
    return this.http.put(url, Land, httpOptions);
  }

  deleteLandById(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/landowners/${id}`;
    return this.http.delete(url, id);
  }
}
