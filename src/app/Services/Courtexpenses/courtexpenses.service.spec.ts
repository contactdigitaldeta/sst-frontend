import { TestBed } from '@angular/core/testing';

import { CourtexpensesService } from './courtexpenses.service';

describe('CourtexpensesService', () => {
  let service: CourtexpensesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourtexpensesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
