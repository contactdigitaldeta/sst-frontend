import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class CourtexpensesService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(
    private http: HttpClient,
  ){
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
  }

  addcourtexpense(Court): Observable<any> {
    // console.log(JSON.stringify(Booking));
    const url = `${this.sstUrl}/courtExpenses`;
    return this.http.post(url, Court, httpOptions);
  }

  getAllCourtExpenses(): Observable<any> {
    //console.log("calling booking by id");
    const url = `${this.sstUrl}/courtExpenses`;
    return this.http.get(url);
  }

  getAllCourtExpensesById(id): Observable<any> {
    //console.log("calling get booking by id");
    const url = `${this.sstUrl}/courtExpenses/${id}`;
    return this.http.get(url,id );
  }

  updatecourtexpenses(Court, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/courtExpenses/${id}`;
    return this.http.put(url, Court, httpOptions);
  }

  deleteCourtexpenses(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/courtExpenses/${id}`;
    return this.http.delete(url, id);
  }
}
