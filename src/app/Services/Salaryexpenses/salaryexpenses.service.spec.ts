import { TestBed } from '@angular/core/testing';

import { SalaryexpensesService } from './salaryexpenses.service';

describe('SalaryexpensesService', () => {
  let service: SalaryexpensesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalaryexpensesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
