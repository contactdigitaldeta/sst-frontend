import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(private http: HttpClient) {
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
   }
  getPublicContent(): Observable<any> {
    const url = `${this.sstUrl}/all`;
    return this.http.get(url, { responseType: 'text' });
  }
  getSupervisorBoard(): Observable<any> {
    const url = `${this.sstUrl}/supervisor`;
    return this.http.get(url, { responseType: 'text' });
  }
  getAdminBoard(): Observable<any> {
    const url = `${this.sstUrl}/admin`;
      return this.http.get(url, { responseType: 'text' });
    }
}
