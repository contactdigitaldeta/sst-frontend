import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from './token-storage.service';



const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  private readonly JWT_TOKEN='JWT_TOKEN'


  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(private http: HttpClient,private storageservice:TokenStorageService) {
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST/api/auth';
   }


   Login(logindata): Observable<any>{
    const url = `${this.sstUrl}/signin`;
    return this.http.post(url, logindata, httpOptions);
   }


   register(signupdata): Observable<any> {
    const url = `${this.sstUrl}/signup`;
    return this.http.post(url , signupdata, httpOptions);
  }

  isLoggedIn(){
    let token = this.storageservice.getToken();
    console.log("token "+ token)
    if(token !== null){
      return true;
    } else
     return false;
  }
  
  getJwtToken(){
    return localStorage.getItem(this.JWT_TOKEN);
  }

  


  



}
