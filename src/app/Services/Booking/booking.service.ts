import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(
    private http: HttpClient,
  ){
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
  }

  addBooking(Booking): Observable<any> {
    // console.log(JSON.stringify(Booking));
    const url = `${this.sstUrl}/bookings`;
    return this.http.post(url, Booking, httpOptions);
  }

  getAllBooking(): Observable<any> {
    //console.log("calling booking by id");
    const url = `${this.sstUrl}/bookings`;
    return this.http.get(url);
  }

  getBookingById(id): Observable<any> {
    //console.log("calling get booking by id");
    const url = `${this.sstUrl}/bookings/${id}`;
    return this.http.get(url, id );
  }

  updateBooking(Booking, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/bookings/${id}`;
    return this.http.put(url, Booking, httpOptions);
  }

  deleteBookingById(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/bookings/${id}`;
    return this.http.delete(url, id);
  }

}
