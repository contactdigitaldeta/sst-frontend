import { Injectable } from "@angular/core";
import {  CanActivate, Router } from "@angular/router";
import { TokenStorageService } from "../LoginSignup/token-storage.service";

@Injectable({
    providedIn: 'root',
  })


export class AuthGuard implements CanActivate {

    constructor(private router: Router,private Tokenservice:TokenStorageService) { }

    
    canActivate() {
        const token_key = this.Tokenservice.getToken();
        // if(!this.loginService.isLoggedIn()){
        //  this.router.navigate(['login']);
        // }
        // return true;
        if(token_key == null){
          this.router.navigate(['login']);
        }
        return true;
      }
    
}