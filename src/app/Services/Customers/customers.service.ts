import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(private http: HttpClient) {
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
   }


   addCustomer(Customer): Observable<any> {
    const url = `${this.sstUrl}/customers`;
    return this.http.post(url, Customer, httpOptions);
  }

  getAllCustomer(): Observable<any> {
    //console.log("calling customer by id");
    const url = `${this.sstUrl}/customers`;
    return this.http.get(url);
  }

  getCustomerById(id): Observable<any> {
    //console.log("calling get customer by id");
    const url = `${this.sstUrl}/customers/${id}`;
    return this.http.get(url, id );
  }

  updateCustomer(Customer, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/customers/${id}`;
    return this.http.put(url, Customer, httpOptions);
  }

  deleteCustomerById(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/customers/${id}`;
    return this.http.delete(url, id);
  }

}
