import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions={
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})


export class ProjectexpensesService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(
    private http: HttpClient,
  ){
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
  }

  addProjectExpenses(ProjectExpense): Observable<any> {
    // console.log(JSON.stringify(Booking));
    const url = `${this.sstUrl}/projectExpences`;
    return this.http.post(url, ProjectExpense, httpOptions);
  }

  getAllProjectExpenses(): Observable<any> {
    //console.log("calling booking by id");
    const url = `${this.sstUrl}/projectExpences`;
    return this.http.get(url);
  }

  getProjectExpensesById(id): Observable<any> {
    //console.log("calling get booking by id");
    const url = `${this.sstUrl}/projectExpences/${id}`;
    return this.http.get(url, id );
  }

  updateProjectExpenses(ProjectExpense, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/projectExpences/${id}`;
    return this.http.put(url, ProjectExpense, httpOptions);
  }

  deleteProjectExpensesById(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/projectExpences/${id}`;
    return this.http.delete(url, id);
  }
}
