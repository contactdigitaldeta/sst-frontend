import { TestBed } from '@angular/core/testing';

import { ProjectexpensesService } from './projectexpenses.service';

describe('ProjectexpensesService', () => {
  let service: ProjectexpensesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectexpensesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
