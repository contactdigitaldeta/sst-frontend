import { TestBed } from '@angular/core/testing';

import { OfficeexpensesService } from './officeexpenses.service';

describe('OfficeexpensesService', () => {
  let service: OfficeexpensesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeexpensesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
