import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class OfficeexpensesService {
  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(
    private http: HttpClient,
  ){
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
  }

  addofficeexpense(Office): Observable<any> {
    // console.log(JSON.stringify(Booking));
    const url = `${this.sstUrl}/officeexpenses`;
    return this.http.post(url, Office, httpOptions);
  }

  getAllOfficeExpenses(): Observable<any> {
    //console.log("calling booking by id");
    const url = `${this.sstUrl}/officeexpenses`;
    return this.http.get(url);
  }

  getAllOfficeExpensesById(id): Observable<any> {
    //console.log("calling get booking by id");
    const url = `${this.sstUrl}/officeexpenses/${id}`;
    return this.http.get(url,id );
  }

  updateofficeexpenses(Office, id): Observable<any> {
    // console.log(JSON.stringify(Customer));
    const url = `${this.sstUrl}/officeexpenses/${id}`;
    return this.http.put(url, Office, httpOptions);
  }

  deleteOfficeexpenses(id): Observable<any>{
    //console.log("calling delete customer by id "+id);
    const url = `${this.sstUrl}/officeexpenses/${id}`;
    return this.http.delete(url, id);
  }



}
