import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class MediatorService {

  private sstHost = `${environment.host}`;
  private sstPort = `${environment.port}`;
  private sstProtocol = `${environment.protocol}`;
  private sstUrl = '';

  constructor(private http: HttpClient) {
    this.sstUrl = this.sstProtocol+'//'+this.sstHost+':'+this.sstPort+'/SST';
   }


   addMediator(Lead): Observable<any> {
    console.log(JSON.stringify(Lead));
    const url = `${this.sstUrl}/mediators`;
    return this.http.post(url, Lead, httpOptions);
  }

  getAllMediator(): Observable<any> {
    console.log("calling lead by id");
    const url = `${this.sstUrl}/mediators`;
    return this.http.get(url);
  }

  getMediatorById(id): Observable<any> {
    console.log("calling get lead by id");
    const url = `${this.sstUrl}/mediators/${id}`;
    return this.http.get(url, id );
  }

  updateMediator(Lead, id): Observable<any> {
    // console.log(JSON.stringify(Lead));
    const url = `${this.sstUrl}/mediators/${id}`;
    return this.http.put(url, Lead, httpOptions);
  }

  deleteMediatorById(id): Observable<any>{
    //console.log("calling delete lead by id "+id);
    const url = `${this.sstUrl}/mediators/${id}`;
    return this.http.delete(url, id);
  }
}
