import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { BookingComponent } from "src/app/sst/Bookings/booking/booking.component";
import { AllBookingComponent } from "src/app/sst/Bookings/all-booking/all-booking.component";
import { AllLandsComponent } from "src/app/sst/Lands/all-lands/all-lands.component";
import { AddLandsComponent } from "src/app/sst/Lands/add-lands/add-lands.component";
import { AllUserComponent } from "src/app/sst/Users/all-user/all-user.component";
import { AddUserComponent } from "src/app/sst/Users/add-user/add-user.component";
import { AddLeadComponent } from "src/app/sst/Leads/add-lead/add-lead.component";
import { AllLeadComponent } from "src/app/sst/Leads/all-lead/all-lead.component";
import { DetailsBookingComponent } from "src/app/sst/Bookings/details-booking/details-booking.component";
import { DetailsLandComponent } from "src/app/sst/Lands/details-land/details-land.component";
import { DetailsUserComponent } from "src/app/sst/Users/details-user/details-user.component";
import { DetailsLeadComponent } from "src/app/sst/Leads/details-lead/details-lead.component";
import { CustomersComponent } from "src/app/sst/Customer/customers/customers.component";
import { AllCustomersComponent } from "src/app/sst/Customer/all-customers/all-customers.component";
import { DetailsCustomersComponent } from "src/app/sst/Customer/details-customers/details-customers.component";
import { MediatorComponent } from "src/app/sst/Mediators/mediator/mediator.component";
import { AllMediatorComponent } from "src/app/sst/Mediators/all-mediator/all-mediator.component";
import { DetailsMediatorComponent } from "src/app/sst/Mediators/details-mediator/details-mediator.component";
import { AllProjectexpensesComponent } from "src/app/sst/ProjectExpenses/all-projectexpenses/all-projectexpenses.component";
import { ProjectexpensesComponent } from "src/app/sst/ProjectExpenses/projectexpenses/projectexpenses.component";
import { DetailsProjectexpensesComponent } from "src/app/sst/ProjectExpenses/details-projectexpenses/details-projectexpenses.component";
import { AllSalaryexpensesComponent } from "src/app/sst/Salaryexpenses/all-salaryexpenses/all-salaryexpenses.component";
import { SalaryexpensesComponent } from "src/app/sst/Salaryexpenses/salaryexpenses/salaryexpenses.component";
import { DetailsSalaryexpensesComponent } from "src/app/sst/Salaryexpenses/details-salaryexpenses/details-salaryexpenses.component";
import { AllOfficeexpensesComponent } from "src/app/sst/Officeexpenses/all-officeexpenses/all-officeexpenses.component";
import { OfficeexpensesComponent } from "src/app/sst/Officeexpenses/officeexpenses/officeexpenses.component";
import { DetailsOfficeexpensesComponent } from "src/app/sst/Officeexpenses/details-officeexpenses/details-officeexpenses.component";
import { AllCourtexpensesComponent } from "src/app/sst/Courtexpenses/all-courtexpenses/all-courtexpenses.component";
import { CourtexpensesComponent } from "src/app/sst/Courtexpenses/courtexpenses/courtexpenses.component";
import { DetailsCourtexpensesComponent } from "src/app/sst/Courtexpenses/details-courtexpenses/details-courtexpenses.component";
import { LoginComponent } from "src/app/sst/Login/login/login.component";
import { SignupComponent } from "src/app/sst/Login/signup/signup.component";
// import { RtlComponent } from "../../pages/rtl/rtl.component";



export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent},
  { path: "booking", component: BookingComponent },
  { path: 'booking/:id',component: BookingComponent },
  { path: "all-booking", component: AllBookingComponent },
  { path: "details-booking", component: DetailsBookingComponent },
  { path: 'details-booking/:id', component: DetailsBookingComponent},
  { path: "all-lands", component: AllLandsComponent },
  { path: "add-lands", component: AddLandsComponent },
  { path: "add-lands/:id", component: AddLandsComponent },
  { path: "details-lands", component: DetailsLandComponent },
  { path: "details-lands/:id", component: DetailsLandComponent },
  { path: "all-user", component: AllUserComponent },
  { path: "add-user", component: AddUserComponent },
  { path: "details-user", component: DetailsUserComponent },
  { path: "add-lead", component: AddLeadComponent },
  { path: "add-lead/:id", component: AddLeadComponent },
  { path: "all-lead", component: AllLeadComponent },
  { path: "details-lead", component: DetailsLeadComponent },
  { path: "details-lead/:id", component: DetailsLeadComponent },
  { path: "customers", component: CustomersComponent},
  { path: "customers/:id", component: CustomersComponent},
  { path: "all-customers", component: AllCustomersComponent },
  { path: "details-customers", component:  DetailsCustomersComponent},
  { path: "details-customers/:id", component:  DetailsCustomersComponent},
  { path: "add-user", component:  AddUserComponent},
  { path: "add-user/:id", component:  AddUserComponent},
  { path: "all-user", component:  AllUserComponent},
  { path: "details-user", component:  DetailsUserComponent},
  { path: "details-user/:id", component:  DetailsUserComponent},
  { path: "mediator", component:  MediatorComponent},
  { path: "mediator/:id", component:  MediatorComponent},
  { path: "all-mediator", component:  AllMediatorComponent},
  { path: "details-mediator", component:  DetailsMediatorComponent},
  { path: "details-mediator/:id", component:  DetailsMediatorComponent},
  {path:"all-projectexpenses",component:AllProjectexpensesComponent},
  {path:"projectexpenses",component:ProjectexpensesComponent},
  {path:"projectexpenses/:id",component:ProjectexpensesComponent},
  {path:"detailsprojectexpenses",component:DetailsProjectexpensesComponent},
  {path:"detailsprojectexpenses/:id",component:DetailsProjectexpensesComponent},
  {path:"all-salaryexpenses",component:AllSalaryexpensesComponent},
  {path:"salaryexpenses",component:SalaryexpensesComponent},
  {path:"salaryexpenses/:id",component:SalaryexpensesComponent},
  {path:"details-salaryexpenses",component:DetailsSalaryexpensesComponent},
  {path:"details-salaryexpenses/:id",component:DetailsSalaryexpensesComponent},
  {path:"all-officeexpense",component:AllOfficeexpensesComponent},
  {path:"officeexpenses",component:OfficeexpensesComponent},
  {path:"officeexpenses/:id",component:OfficeexpensesComponent},
  {path:"details-officeexpense",component:DetailsOfficeexpensesComponent},
  {path:"details-officeexpense/:id",component:DetailsOfficeexpensesComponent},
  {path:"all-courtexpense",component:AllCourtexpensesComponent},
  {path:"courtexpenses",component:CourtexpensesComponent},
  {path:"courtexpenses/:id",component:CourtexpensesComponent},
  {path:"details-courtexpense",component:DetailsCourtexpensesComponent},
  {path:"details-courtexpense/:id",component:DetailsCourtexpensesComponent},
  {path:"login",component:LoginComponent},
  {path:"signup",component:SignupComponent},
];
