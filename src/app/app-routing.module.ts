import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { LoginComponent } from "./sst/Login/login/login.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { SignupComponent } from "./sst/Login/signup/signup.component";
import { AuthGuard } from "./Services/Guards/authguard";


const routes: Routes=[
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
   
    // children: [
    //   {
    //     path: "",
        
    //     loadChildren:
    //       "./layouts/admin-layout/admin-layout.module#AdminLayoutModule",

    //   }
    // ]
  },
  { 
    path:'login',
    component: LoginComponent
  },
  {
    path:'signup',
    component: SignupComponent,
    // canActivate : [GuardsComponent],

  },
  { 
    path: "",
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",      
        loadChildren:
          "./layouts/admin-layout/admin-layout.module#AdminLayoutModule",
      }
    ],
    
  },
  { path : 'dashboard', 
    component: DashboardComponent,
    canActivate : [AuthGuard],
  }
  // {
  //   path: "**",
  //   redirectTo: "dashboard"
  // },
]

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
