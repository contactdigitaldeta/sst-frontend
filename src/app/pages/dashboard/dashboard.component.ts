import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import Chart from 'chart.js';
import { TokenStorageService } from "src/app/Services/LoginSignup/token-storage.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "dashboard.component.html",
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public canvas : any;
  public ctx;
  public datasets: any;
  public data: any;
  public myChartData;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public clicked2: boolean = false;


  bookingCount:number=0;
  bookingCountstop:any=setInterval(()=>{
    this.bookingCount++;
    if(this.bookingCount==50){
      clearInterval(this.bookingCountstop);
    }
  },30)
  landowner:number=0;
  landownerstop:any=setInterval(()=>{
    this.landowner++;
    if(this.landowner==50){
      clearInterval(this.landownerstop);
    }
  },30)
  lead:number=0;
  leadstop:any=setInterval(()=>{
    this.lead++;
    if(this.lead==80){
      clearInterval(this.leadstop);
    }
  },30)
  customer:number=0;
  customerstop:any=setInterval(()=>{
    this.customer++;
    if(this.customer==60){
      clearInterval(this.customerstop);
    }
  },30)
  brokers:number=0;
  brokerstop:any=setInterval(()=>{
    this.brokers++;
    if(this.brokers==50){
      clearInterval(this.brokerstop);
    }
  },30)
  
  constructor(private Tokenservice:TokenStorageService,private router:Router) {}

  ngOnInit() {
    const token_key = this.Tokenservice.getToken();
    if(token_key == null){
      this.router.navigate(['login']);
    }
  }
}
