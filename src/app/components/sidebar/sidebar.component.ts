import { Component, OnInit } from "@angular/core";
import { NgbModule }
from '@ng-bootstrap/ng-bootstrap';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "icon-chart-pie-36",
    class: ""
  },
  {
    path: "/all-booking",
    title: "Booking",
    icon: "icon-paper",
    class: ""
  },
  {
    path: "/all-lands",
    title: "Land Owner",
    icon: "icon-badge",
    class: ""
  },
  {
      path: "/all-lead",
      title: "Leads",
      icon: "icon-user-run",
      class: ""
    },
    {
      path: "/all-customers",
      title: "Customers",
      icon: "icon-satisfied", 
      class: ""
    },
    {
      path: "/all-user",
      title: "Broker",
      icon: "icon-support-17",
      class: ""
    },
    {
      path: "/all-mediator",
      title: "Mediator",
      icon: "icon-single-02",
      class: ""
    },
    // {
    //   path:"/all-projectexpenses",
    //   title:"Project Expenses",
    //   icon:"icon-single-02",
    //   class:""
    // },
    // {
    //   path:"/all-salaryexpenses",
    //   title:"Salary Expenses",
    //   icon:"icon-single-02",
    //   class:""
    // },
    // {
    //   path:"/all-officeexpense",
    //   title:"Office Expenses",
    //   icon:"icon-single-02",
    //   class:""
    // },
    // {
    //   path:"/all-courtexpense",
    //   title:"Court Expenses",
    //   icon:"icon-single-02",
    //   class:""
    // },



];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  IsDashboard: boolean=false;

  constructor() {}

  ngOnInit() {

    // if(routerLink="")
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
